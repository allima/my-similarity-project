package br.edu.ifsp.similarity.file;

public enum TipoArquivo {

	TXT("txt"), PDF("pdf"), WORD("word"), PTT("ppt"), EXCEL("excel");
	public String tipoAruivo;

	TipoArquivo(String tipo) {
		tipoAruivo = tipo;
	}
}
