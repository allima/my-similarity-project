package br.edu.ifsp.similarity.file;

public class FabricaArquivo {

	public Arquivo getArquivos(TipoArquivo tipoArquivo) {

		if (tipoArquivo == null) {
			return null;
		}

		if (TipoArquivo.PDF.equals(tipoArquivo)) {
			return new ArquivoPDF();
		} else if (TipoArquivo.EXCEL.equals(tipoArquivo)) {
			return new ArquivoExcel();
		} else if (TipoArquivo.TXT.equals(tipoArquivo)) {
			return new ArquivoTXT();
		} else if (TipoArquivo.PTT.equals(tipoArquivo)) {
			return new ArquivoPTT();
		}
		return null;
	}

}
