package br.edu.ifsp.similarity;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.apache.log4j.Logger;

import junit.framework.TestCase;

public class PrincipalTest extends TestCase {

	final static Logger logger = Logger.getLogger(PrincipalTest.class);

	protected void setUp() throws Exception {
		super.setUp();
	}

	public final void testFunfa() {
		// Prepare to capture output
		PrintStream originalOut = System.out;
		OutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);
		System.setOut(ps);
		String[] args = null;
		// Perform tests
		Principal.main(args);
		assertEquals("funfa".trim(), os.toString().trim());
		// Restore normal operation
		System.setOut(originalOut);
		logger.debug("This is debug : ");
	}
}
